from celery import Celery
from digivlacelery.constants import BROADCAST_RECEIVE_POST
from kombu.common import Broadcast

class MessagePostReceiveConfig:
    # List of modules to import when celery starts.
    CELERY_ACCEPT_CONTENT = ['json']
    # CELERY_IMPORTS = ('main.tasks')
    CELERY_QUEUES = (Broadcast('post_receive'),)
    CELERY_ROUTES = {
        BROADCAST_RECEIVE_POST : {'queue': 'post_receive'}
    }

       

class MessagePostReceive(object):
    def __init__(self, broker_url):
        self.app = Celery('digivla')
        self.broker_url = broker_url
        self.apply_config()
        
        
    def get_app(self):
        return self.app

    def apply_config(self):
        self.app.config_from_object(MessagePostReceiveConfig())
        self.app.conf.broker_url = self.broker_url
        self.app.conf.broker_connection_max_retries=3






        
