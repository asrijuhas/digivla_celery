from celery import Celery
from digivlacelery.constants import DIRECT_TWITTER_USER_SHORT, DIRECT_TWITTER_LOCATION_SHORT
from kombu import Exchange, Queue

       

class MessageDirectTwitterUserShort(object):
    def __init__(self, broker_url):
        self.app = Celery("trinix")
        self.broker_url = broker_url
        self.apply_config()
        
        
    def get_app(self):
        return self.app

    def apply_config(self):
        self.app.conf.broker_url = self.broker_url
        self.app.conf.task_default_exchange = "twitter"
        self.app.conf.task_default_queue ="twiter_user_short"
        self.app.conf.broker_connection_max_retries=3
        self.app.conf.task_default_routing_key=DIRECT_TWITTER_USER_SHORT
        self.app.conf.tasks_queues=(
            Queue('twiter_user_short', Exchange('twitter'), routing_key=DIRECT_TWITTER_USER_SHORT),
            
        )

class MessageDirectTwitterLocationsShort(object):
    def __init__(self, broker_url):
        self.app = Celery("trinix")
        self.broker_url = broker_url
        self.apply_config()
        
        
    def get_app(self):
        return self.app

    def apply_config(self):
        self.app.conf.broker_url = self.broker_url
        self.app.conf.task_default_exchange = "twitter"
        self.app.conf.task_default_queue ="twiter_location_short"
        self.app.conf.broker_connection_max_retries=3
        self.app.conf.task_default_routing_key=DIRECT_TWITTER_LOCATION_SHORT
        self.app.conf.tasks_queues=(
            Queue('twiter_location_short', Exchange('twitter'), routing_key=DIRECT_TWITTER_LOCATION_SHORT),
            
        )

        
