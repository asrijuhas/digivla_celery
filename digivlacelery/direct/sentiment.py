from celery import Celery
from digivlacelery.constants import  DIRECT_SENTIMENT_DATA, DIRECT_SENTIMENT_RESULT
from kombu import Exchange, Queue

       

class MessageDirectSentimentData(object):
    def __init__(self, broker_url):
        self.app = Celery("trinix")
        self.broker_url = broker_url
        self.apply_config()
        
        
    def get_app(self):
        return self.app

    def apply_config(self):
        self.app.conf.broker_url = self.broker_url
        self.app.conf.task_default_exchange = "sentiment"
        self.app.conf.task_default_queue ="sentiment_data"
        self.app.conf.broker_connection_max_retries=3
        self.app.conf.task_default_routing_key=DIRECT_SENTIMENT_DATA
        self.app.conf.tasks_queues=(
            Queue('sentiment_data', Exchange('sentiment'), routing_key=DIRECT_SENTIMENT_DATA),
            
        )

class MessageDirectSentimentResult(object):
    def __init__(self, broker_url):
        self.app = Celery("trinix")
        self.broker_url = broker_url
        self.apply_config()
        
        
    def get_app(self):
        return self.app

    def apply_config(self):
        self.app.conf.broker_url = self.broker_url
        self.app.conf.task_default_exchange = "sentiment"
        self.app.conf.task_default_queue ="sentiment_result"
        self.app.conf.task_default_routing_key=DIRECT_SENTIMENT_RESULT
        self.app.conf.broker_connection_max_retries=3
        self.app.conf.tasks_queues=(
            Queue('sentiment_result', Exchange('sentiment'), routing_key=DIRECT_SENTIMENT_RESULT),
            
        )

