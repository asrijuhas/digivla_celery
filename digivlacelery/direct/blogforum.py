from celery import Celery
from digivlacelery.constants import DIRECT_LABEL_BLOGFORUM
from kombu import Exchange, Queue

class MessageLabelBlogForum(object):
    def __init__(self, broker_url):
        self.app = Celery("trinix")
        self.broker_url = broker_url
        self.apply_config()
    
    def get_app(self):
        return self.app

    def apply_config(self):
        self.app.conf.broker_url = self.broker_url
        self.app.conf.task_default_exchange = "blogforum"
        self.app.conf.task_default_queue ="label_blogforum"
        self.app.conf.task_default_routing_key=DIRECT_LABEL_BLOGFORUM
        self.app.conf.broker_connection_max_retries=3
        self.app.conf.tasks_queues=(
            Queue('label_blogforum', Exchange('blogforum'), routing_key=DIRECT_LABEL_BLOGFORUM),
            
        )