from celery import Celery
from digivlacelery.constants import  DIRECT_POST, DIRECT_GET_POST_DETAIL
from kombu import Exchange, Queue

       

class MessageDirectPostData(object):
    def __init__(self, broker_url):
        self.app = Celery("trinix")
        self.broker_url = broker_url
        self.apply_config()
        
        
    def get_app(self):
        return self.app

    def apply_config(self):
        self.app.conf.broker_url = self.broker_url
        self.app.conf.task_default_exchange = "post"
        self.app.conf.task_default_queue ="post_data"
        self.app.conf.task_default_routing_key=DIRECT_POST
        self.app.conf.broker_connection_max_retries=3
        self.app.conf.tasks_queues=(
            Queue('post_data', Exchange('post'), routing_key=DIRECT_POST),
            
        )


class MessageDirectGetPostDetail(object):
    def __init__(self, broker_url):
        self.app = Celery("trinix")
        self.broker_url = broker_url
        self.apply_config()
        
        
    def get_app(self):
        return self.app

    def apply_config(self):
        self.app.conf.broker_url = self.broker_url
        self.app.conf.task_default_exchange = "get_post"
        self.app.conf.task_default_queue ="get_post_queue"
        self.app.conf.task_default_routing_key=DIRECT_POST
        self.app.conf.broker_connection_max_retries=3
        self.app.conf.tasks_queues=(
            Queue('get_post_queue', Exchange('get_post'), routing_key=DIRECT_POST),
            
        )