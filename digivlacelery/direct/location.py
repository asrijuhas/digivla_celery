from celery import Celery
from digivlacelery.constants import DIRECT_USER_LOCATION
from kombu import Exchange, Queue

class MessageDirectUserLocation(object):
    def __init__(self, broker_url):
        self.app = Celery("trinix")
        self.broker_url = broker_url
        self.apply_config()
        
        
    def get_app(self):
        return self.app

    def apply_config(self):
        self.app.conf.broker_url = self.broker_url
        self.app.conf.task_default_exchange = "userlocation"
        self.app.conf.task_default_queue ="userlocation"
        self.app.conf.broker_connection_max_retries=3
        self.app.conf.task_default_routing_key=DIRECT_USER_LOCATION
        self.app.conf.tasks_queues=(
            Queue('userlocation', Exchange('userlocation'), routing_key=DIRECT_USER_LOCATION),
            
        )