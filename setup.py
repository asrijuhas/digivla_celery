import setuptools

with open("README.md") as fh:
    long_description=fh.read()


setuptools.setup(
    name="digivlacelery",
    version="0.0.1",
    scripts=['init'],
    author="asri djufri",
    author_email="asri@trinix.id",
    description="base library for digivla sna messaging using celery",
    long_description = long_description,
    url="https://bitbucket.org/asrijuhas/digivla_celery",
    packages=setuptools.find_packages(),
    install_requires=[
        'celery==4.4.0'
    ],
    classifiers=[
        "Program language : python : 3.7 ",
        "Project: digivla sna"
    ]
)